const express = require('express');
const expressLogging = require('express-logging');
const logger = require('logops');

const app = express();

// Define the port to run on
logger.format = logger.formatters.dev;
app.set('port', 8085);
app.use(expressLogging(logger));

const allowCrossDomain = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Methods',
    'GET,PUT,POST,DELETE,OPTIONS,TRACE,HEAD',
  );
  res.header(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization, Content-Length, X-Requested-With',
  );
  // intercept OPTIONS method
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
};
app.use(allowCrossDomain);

// 請求路徑
app.use(express.static('src/main/resources'));

// Listen for requests
const server = app.listen(app.get('port'), () => {
  const port = server.address().port;
  console.log(`Magic happens on port ${port}`);
});
